from flask import Flask
from flask import jsonify
from flask import request

from tasks.task_factory import TaskFactory


class RestServer:
    _self = None
    app = Flask(__name__)

    # Ensure a singleton
    def __new__(cls, **kwargs):
        if cls._self is None:
            cls._self = super().__new__(cls)
            cls._self.__initialized = False
        return cls._self

    def __init__(self, host='0.0.0.0', port=105):
        if (self.__initialized): return
        self.__initialized = True
        self.api_host = host
        self.api_port = port
        self.tasks = [] # should be protected by a Lock

    def get_task(self, task_id):
        for t in self.tasks:
            if t.id == task_id:
                return t.status()
        return {'error': 'No task found with given ID'}

    def start(self):
        RestServer.app.run(self.api_host, self.api_port)

    """ API's FUNCTIONALITIES """

    @staticmethod
    @app.route('/hello/', methods=['GET', 'POST'])
    def welcome():
        return "Hello, My name is VrAcPI! I'm a small restfull API developed in Python using Flask." \
               "I have no useful purpose other than allowing someone to test stuff around APIs :)"

    @staticmethod
    @app.route('/status/', methods=['GET'])
    def status():
        return jsonify({'state': 'Running', 'info': 'Obviously if I was not running, I would not have responded!'})

    @staticmethod
    @app.route('/startTask/<string:name>', methods=['POST'])
    def start_task(name):
        task_instance = TaskFactory.new(name, request.args)
        if task_instance.id:
            task_instance.launch()
            RestServer().tasks.append(task_instance)


        return task_instance.status()

    @staticmethod
    @app.route('/getTaskStatus/<string:id>', methods=['GET'])
    def get_task_status(id):
        return RestServer().get_task(id)


