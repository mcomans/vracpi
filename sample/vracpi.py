

from sample.rest_server import RestServer

if __name__ == '__main__':
    server = RestServer()

    # Do setup stuff if needed here ...

    # Start API
    server.start()
