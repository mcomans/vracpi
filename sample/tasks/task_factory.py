from tasks.task_count_to import CountTo
from tasks.task_instance import TaskInstance


class TaskFactory:
    @staticmethod
    def new(name, args):
        if name == "countTo":
            validation = CountTo.validate(args)
            return CountTo(name, float(args.get('target'))) if validation[0] else DefaultTask(name, validation[1])
        else:
            return DefaultTask(name, "Not a valid task")


class DefaultTask(TaskInstance):
    def __init__(self, name, err_msg):
        super().__init__(name)
        self.id = None
        self.err_msg = err_msg

    def status(self):
        return {'name': self.name, 'error': self.err_msg}