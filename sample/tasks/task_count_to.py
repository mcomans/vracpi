import time

from tasks.task_instance import TaskInstance


class CountTo(TaskInstance):
    def __init__(self, name, target=180):
        super().__init__(name)
        self.target = target
        self.index = 1

    @staticmethod
    def validate(args):
        try:
            return isinstance(float(args['target']), float), ""
        except KeyError:
            print('target not in arguments')
            return False, 'target parameter missing !'
        except ValueError:
            return False, f"Not an Number : {args['target']}"

    def run(self):
        if self.target < self.index:
            raise ValueError(f"Cannot count incrementally to {self.target}!")
        while self.index < self.target:
            self.index += 1
            time.sleep(1)

    def status(self):
        status = super().status()
        status.update({'index': self.index})

        return status
