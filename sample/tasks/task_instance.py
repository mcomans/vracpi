import hashlib
import time
import threading

class TaskInstance:
    def __init__(self, name):
        self.name = name
        self.running = False
        self.terminated = False
        self.id = hashlib.sha1(f"{self.name} {round(time.time())}".encode('utf-8')).hexdigest()

    @staticmethod
    def validate(args):
        return True

    def state(self):
        if self.terminated:
            return 'KO' if self.running else 'DONE'
        else:
            return 'RUNNING' if self.running else 'INCOMING'

    def status(self):
        return {'name': self.name, 'state': self.state(), 'id':self.id}

    def run(self):
        raise NotImplemented

    def status_run(self):
        try:
            self.run()
            self.running = False
        except Exception as e:
            print(f"An error occurred during the execution of the task: {e}")
        self.terminated = True

    def launch(self):
        self.running = True
        t = threading.Thread(target=self.status_run)
        t.start()


