import unittest
import time

from tasks.task_count_to import CountTo


class TaskCountToTest(unittest.TestCase):

    def test_init(self):
        task = CountTo("foo")
        self.assertEqual(task.target, 180)
        task = CountTo("foo", 20)
        self.assertEqual(task.target, 20)
        self.assertEqual(task.state(), "INCOMING")
        self.assertDictEqual(task.status(), {'name': 'foo', 'state': 'INCOMING', 'id': task.id, 'index': 1})

    def test_run(self):
        task = CountTo("foo", 5)
        self.assertEqual(task.state(), "INCOMING")
        task.launch()
        self.assertEqual(task.state(), "RUNNING")
        time.sleep(5.5)
        self.assertEqual(task.state(), "DONE")
        self.assertEqual(task.index, 5)

    def test_fail(self):
        task = CountTo("foo", -5)
        self.assertEqual(task.state(), "INCOMING")
        task.launch()
        time.sleep(0.5)
        self.assertEqual(task.state(), "KO")
        self.assertEqual(task.index, 1)

