import unittest

from tasks.task_factory import TaskFactory


class TaskFactoryTest(unittest.TestCase):

    def test_task_count_to(self):
        task = TaskFactory.new("countTo", {'target': 20})
        self.assertEqual(task.target, 20)
        self.assertEqual(task.state(), "INCOMING")

        task = TaskFactory.new("countTo", {'max': 20})
        self.assertDictEqual(task.status(), {'name': 'countTo', 'error': 'Bad param!'})


    def test_invalid_task(self):
        task = TaskFactory.new("countFrom", 20)
        self.assertDictEqual(task.status(), {'name': 'countFrom', 'error': 'Not a valid task'})
