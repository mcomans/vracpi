import unittest

from rest_server import RestServer


class RestServerTest(unittest.TestCase):

    def test_server_singleton(self):
        server1 = RestServer(host="example.com", port=1234)
        server2 = RestServer(host="random.net", port=5678)
        self.assertEqual(server1, server2)

        # Since there is only 1 server, another one cannot reconfigure it with a new initialization
        self.assertEqual(server2.api_host, "example.com")
