import argparse
import requests

def parse_args():
    msg = """
    Small utility to make POST and GET requests like curl in Linux that protects the user 
    from headaches caused by windows confusing commands ¯\_(ツ)_/¯
    """
    parser = argparse.ArgumentParser(description=msg)
    parser._action_groups.pop()
    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')
    required.add_argument('url', metavar='--url', nargs=1, type=str, help="Uri to connect to")
    optional.add_argument('-d', '--data', help='body as key:value or json')
    optional.add_argument('-H', '--header', help='Header as key:value or json')
    optional.add_argument('-P', '--port', help='Port number')
    optional.add_argument('-f', '--fail', help='Only output the error code in case of Fail')
    optional.add_argument('-X', '--method', help='Http method: GET/POST', choices=["POST", "GET"])
    optional.add_argument('-g', '--get', action='store_true')
    optional.add_argument('-p', '--post', action='store_true')
    optional.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()

    if args.method and (args.get or args.post): raise SyntaxError("Cannot GET and POST simultaneously")
    if not (args.method or args.get or args.post): args.get = True # default is GET
    args.url = validate_url(args.url[0], args.port)

    return args


def validate_url(url, port):
    url_parts = url.split(':')
    if url_parts[0].startswith('http'):
        if len(url_parts) == 2:
            # port number not specified
            if not port:
                port = 443 if 'https' in url_parts[0] else 80
            _uris = url_parts[1].lstrip('//').split('/')
            return f"{url_parts[0]}://{_uris[0]}:{port}/" + '/'.join(_uris[1:])
        if len(url_parts) == 3:
            # port number specified
            _uris = url_parts[2].split('/')
            if port:
                _uris[0] = str(port)
            return f"{url_parts[0]}://{url_parts[1]}:" + '/'.join(_uris)
    # If nothing has been returned, the url is not accepted
    raise SyntaxError("Not a valid URL")


def send_request(args):
    if args.get or args.method == "GET":
        r = requests.get(args.url)

    elif args.post or args.method == "POST":
        r = requests.post(args.url)

    else:
        print("How did we get here ?")
        r = None

    if not args.fail:
        r.raise_for_status()
    print(r.status_code)
    if not args.fail:
        print(r.content.decode())


if __name__ == '__main__':
    args = parse_args()
    if args.verbose:
        print(args)
    send_request(args)
