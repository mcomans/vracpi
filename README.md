# VrAcPI
VrAcPI is a minimalist REST API with the purpose of allowing someone to test some stuff around the topic. 
'Vrac' is the keyword I use in French when I want to say 'project sandbox' ;)

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the python requirements and export /sample into your PYTHON PATH

```bash
# Always good practice to use a virtual env
## LINUX ##
python -m venv venv/
source venv/bin/activate
pip install -r requirements.txt
export PYTHONPATH=$PYTHONPATH:$PWD/sample

## WINDOWS ## 

# I wanted to write the lines for Windows as well but it's so less straight forward and way more annoying than with 
# linux that it's not worth wasting my time finding the correct syntax even though I spent time writing this useless comment :p 
```
## Usage

```bash
# starts the API
python sample/vracpi.py


# (In a separate Terminal)
# bin/wcurl.py is a custom script mimicking curl on linux I made to keep me from banging my head using Windows own commands
# returns a welcoming message 
python bin/wcurl.py http:localhost:105/hello

# returns the state of the API
python bin/wcurl.py http:localhost:105/status

# Starts a new job on the server on a separate thread (POST method)
python bin/wcurl.py -p http:localhost:105/startTask/countTo?target=50
# {"id":"16fa8c8b088190c4e752178548385c70b82c13f6","index":0,"name":"countTo","state":"RUNNING"}

# returns the status of the task given its id
python bin/wcurl.py http:localhost:105/getTaskStatus/16fa8c8b088190c4e752178548385c70b82c13f6
# {"id":"16fa8c8b088190c4e752178548385c70b82c13f6","index":37,"name":"countTo","state":"RUNNING"}
# ...
# {"id":"16fa8c8b088190c4e752178548385c70b82c13f6","index":50,"name":"countTo","state":"DONE"}
```
## Contributing

Are people even visiting this repo ? If somehow that's the case, feel free to use any of this. There is nothing much to contribute about other than having fun coding with APIs :)

If you are in need of ideas to test around your skills, try 
- Adding tasks
- Supporting a SQL like Database
- Writing a better code coverage 
- Adding instructions to allow filters on task state, name, etc
- Playing around with Gitlab CI-CI
- Getting your own ideas 'cause I'm starting to run short...


## License

[lol]